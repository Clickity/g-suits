# G-Suits Camera Class

# |Imports|

import bge
import math
import mathutils

# |Definitions|



# |Class Definition|

class camera_controller:
	
	def __init__(self, name, start_distance = 10, angle = 50):
		print("Camera initialized.")
		
		
		self.angle = angle
		self.distance = start_distance
		self.camera_object = bge.logic.getCurrentScene().objects[name]
		bge.logic.getCurrentScene().active_camera = bge.logic.getCurrentScene().objects[name]
		self.camera_object.worldOrientation = [math.radians(angle), math.radians(0), math.radians(90)]
		
		
		# |Configuration Variables|
		
		self.zoom_in_fov = 0.2
		self.zoom_out_fov = 0.2
		self.zoom_min = 10
		self.zoom_max = 100
		self.zoom_in_speed = 0.05
		self.zoom_out_speed = 0.05
		
		
	def update(self, player):
		# Setting horizontal position to be the center position
		# of all the players.
		num_players = 0
		sum_y_pos = 0
		sum_z_pos = 0
		for p in player:
			num_players += 1
			sum_y_pos += p.main_object.position.y
			sum_z_pos += p.main_object.position.z
			
		new_y = sum_y_pos / num_players
		if math.fabs(self.camera_object.position.y - new_y) > 0.05:	
			self.camera_object.position.y = new_y
		
		# Zoom.
		largest_point = [0.0, 0.0]
		has_zoomed_out = False
		for p in player:
			point = self.camera_object.getScreenPosition(p.main_object)
			
			# Zooming out.
			if point[0] < self.zoom_out_fov or point[0] > (1-self.zoom_out_fov) or point[1] < self.zoom_out_fov or point[1] > (1-self.zoom_out_fov):
				self.distance += self.zoom_out_speed
				has_zoomed_out = True
				break
				
			# Finding biggest fov for zoom in.
			if point[0] > largest_point[0]:
				largest_point[0] = point[0]
			if point[1] > largest_point[1]:
				largest_point[1] = point[1]
			
		if largest_point[0] > (0.5-self.zoom_in_fov) and largest_point[0] < (0.5+self.zoom_in_fov) and largest_point[1] > (0.5-self.zoom_in_fov) and largest_point[1] < (0.5+self.zoom_in_fov) and has_zoomed_out == False:
			self.distance -= self.zoom_in_speed
			
		if self.distance > self.zoom_max:
			self.distance = self.zoom_max
		if self.distance < self.zoom_min:
			self.distance = self.zoom_min
			
		
		new_x = self.distance * math.sin(math.radians(self.angle))
		new_z = sum_z_pos / num_players + self.distance * math.cos(math.radians(self.angle))
		
		if math.fabs(self.camera_object.position.x - new_x) > 0.05:
			self.camera_object.position.x = new_x
			
		if math.fabs(self.camera_object.position.z - new_z) > 0.05:
			self.camera_object.position.z = new_z