# G-Suits Config File
# Use this file to change how the game works.

# |Object Names|

# The name of the camera used to view players.
camera_name = "main_camera"

# Player spawns.
player1_spawn_name = "spawn_player1"
player2_spawn_name = "spawn_player2"
player3_spawn_name = "spawn_player3"
player4_spawn_name = "spawn_player4"

# Level spawn modifiers.
orbital_level = "_orbital_level"
colony_level = "_colony_level"
mining_level = "_mining_level"

# |Player Settings|

# The speed the player moves at.
player_movement_speed = 0.125

# The speed the player turns at.
player_spin_speed = 10.0

# The speed the player jumps at.
player_jump_speed = 500

# The player can move while in the air.
player_allow_move_in_air = True

# The player can double jump.
player_allow_double_jump = True

# The distance that the player can pick things up from.
player_pickup_range = 7.5

# The distance to cast the rays down to see if there is floor beneath the 
# astronaut.
player_ground_sensor_length = 0.4

# Lives a player has.
player_lives = 3



# |Player Keybindings|

# These will manually configure which
# button activates an action.
# Default of -1 means that there is no input.
# Possible values can be found at:
# http://www.blender.org/documentation/blender_python_api_2_69_1/bge.events.html#keyboard-keys

# Import so u can set the values.
import bge

# Player 1
player1_move_left = bge.events.AKEY
player1_move_right = bge.events.DKEY
player1_jump = bge.events.WKEY
player1_crouch = bge.events.SKEY
player1_pickup = bge.events.EKEY
player1_throw = bge.events.QKEY

# Player 2
player2_move_left = bge.events.LEFTARROWKEY
player2_move_right = bge.events.RIGHTARROWKEY
player2_jump = bge.events.UPARROWKEY
player2_crouch = bge.events.DOWNARROWKEY
player2_pickup = bge.events.ENDKEY
player2_throw = bge.events.RIGHTSHIFTKEY

# Player 3
player3_move_left = bge.events.JKEY
player3_move_right = bge.events.LKEY
player3_jump = bge.events.IKEY
player3_crouch = bge.events.KKEY
player3_pickup = bge.events.OKEY
player3_throw = bge.events.UKEY

# Player 4
player4_move_left = bge.events.FKEY
player4_move_right = bge.events.HKEY
player4_jump = bge.events.TKEY
player4_crouch = bge.events.GKEY
player4_pickup = bge.events.YKEY
player4_throw = bge.events.RKEY


# Player Controller
from gsuits_controller import *

player_move_left_cont = button.JOYSTICK1_LEFT
player_move_right_cont = button.JOYSTICK1_RIGHT
player_jump_cont = button.JOYSTICK1_UP
player_crouch_cont = button.JOYSTICK1_DOWN
player_pickup_cont = button.A
player_throw_cont = button.B

# |Projectile Settings|

# When a projectile is thrown with an x dimension that is not at 0. The
# correction speed will determine how fast it moves towards 0.
# Note: This is used to keep projectiles flying in 2 dimensions.
projectile_correction_speed = 2.5

# The speed the spear is thrown at.
spear_throw_speed = 30.0

# The speed the crate is thrown at.
crate_throw_speed = 15.0


# |Camera Settings|

# The distance away the camera starts at.
camera_start_distance = 80

# The angle that the camera views from.
# 0 looks straight down.
# 90 looks horizontally.
camera_angle = 72.5

# The speed that the zoom in is adjusted at.
zoom_in_speed = 0.1

# The speed that the zoom out is adjusted at.
zoom_out_speed = 0.05

# Determines the margin between the screen edge and a player
# that is allowed before the camera zooms out.
# 0.0 is the edge of the screen.
# 0.5 is the middle of the screen. Note: You don't want this.
zoom_out_fov = 0.1

# Deteremines the margin between the screen center and the furthest
# player that is allowed before the screen starts zooming in.
# 0.5 is the edge of the screen. Ex. If there are 2 players, then
# the camera will zoom in until they are both just off screen.
# 0.0 is no zoom in.
zoom_in_fov = 0.2

# The closest that the camera can zoom in.
zoom_min = 25

# The furthest the camera can zoom out.
zoom_max = 100


