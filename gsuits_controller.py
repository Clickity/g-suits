# G-Suits Controller

# A useable controller class.

# |Imports|

import bge
import math
import mathutils

# |Definitions|
PRESSED = bge.logic.KX_INPUT_JUST_ACTIVATED
HELD_DOWN = bge.logic.KX_SENSOR_ACTIVE
HELD_UP = bge.logic.KX_SENSOR_INACTIVE
UN_PRESSED = bge.logic.KX_SENSOR_JUST_DEACTIVATED

# |Class Definitions|

class button:
	A = 0
	B = 1
	X = 2
	Y = 3
	LB = 4
	RB = 5
	BACK = 6
	START = 7
	JOYSTICK1_UP = 8
	JOYSTICK1_DOWN = 9
	JOYSTICK1_LEFT = 10
	JOYSTICK1_RIGHT = 11
	JOYSTICK2_UP = 12
	JOYSTICK2_DOWN = 13
	JOYSTICK2_LEFT = 14
	JOYSTICK2_RIGHT = 15	
	ARROWS_UP = 16
	ARROWS_DOWN = 17
	ARROWS_LEFT = 18
	ARROWS_RIGHT = 19
	LEFT_TRIGGER = 20
	RIGHT_TRIGGER = 21


class controller:
	
	# @returns the number of active controllers.
	@staticmethod
	def get_num_of():
		num_controllers = 0
		for j in bge.logic.joysticks:
			if j != None:
				num_controllers += 1	
		return num_controllers
	
	# @param index the controller slot to check for
	#              activity.
	# @returns True if the controller index is active.
	@staticmethod
	def is_active(index):
		if bge.logic.joysticks[index] != None:
			return True
		else:
			return False
	
	# Constructor
	def __init__(self, cont_number):
		self.cont_number = cont_number
		self.device = bge.logic.joysticks[cont_number]
		self.sensitivity = 0.3
		
		self.events  = []
		for i in range(22):
			self.events.append(HELD_UP)
		
	# Updates the atate of the controller so that
	# events read the correct value.
	def update(self):	
		
		# Activating held down.
		for i in range(22):
			if self.events[i] == PRESSED:
				self.events[i] = HELD_DOWN
			
			elif self.events[i] == UN_PRESSED:
				self.events[i] = HELD_UP
			
		
		# Activating/Deactivating buttons.
		if button.A in self.device.activeButtons:
			if self.events[button.A] == HELD_UP:
				self.events[button.A] = PRESSED			
		else:
			if self.events[button.A] == HELD_DOWN:
				self.events[button.A] = UN_PRESSED		
		if button.B in self.device.activeButtons:
			if self.events[button.B] == HELD_UP:
				self.events[button.B] = PRESSED			
		else:
			if self.events[button.B] == HELD_DOWN:
				self.events[button.B] = UN_PRESSED				
		if button.X in self.device.activeButtons:
			if self.events[button.X] == HELD_UP:
				self.events[button.X] = PRESSED			
		else:
			if self.events[button.X] == HELD_DOWN:
				self.events[button.X] = UN_PRESSED
		if button.Y in self.device.activeButtons:
			if self.events[button.Y] == HELD_UP:
				self.events[button.Y] = PRESSED			
		else:
			if self.events[button.Y] == HELD_DOWN:
				self.events[button.Y] = UN_PRESSED
		if button.LB in self.device.activeButtons:
			if self.events[button.LB] == HELD_UP:
				self.events[button.LB] = PRESSED			
		else:
			if self.events[button.LB] == HELD_DOWN:
				self.events[button.LB] = UN_PRESSED				
		if button.RB in self.device.activeButtons:
			if self.events[button.RB] == HELD_UP:
				self.events[button.RB] = PRESSED			
		else:
			if self.events[button.RB] == HELD_DOWN:
				self.events[button.RB] = UN_PRESSED		
		if button.BACK in self.device.activeButtons:
			if self.events[button.BACK] == HELD_UP:
				self.events[button.BACK] = PRESSED			
		else:
			if self.events[button.BACK] == HELD_DOWN:
				self.events[button.BACK] = UN_PRESSED
		if button.START in self.device.activeButtons:
			if self.events[button.START] == HELD_UP:
				self.events[button.START] = PRESSED			
		else:
			if self.events[button.START] == HELD_DOWN:
				self.events[button.START] = UN_PRESSED				
		
		
		
		
		if self.device.axisValues[0] < -self.sensitivity:
			if self.events[button.JOYSTICK1_LEFT] == HELD_UP:
				self.events[button.JOYSTICK1_LEFT] = PRESSED			
		else:
			if self.events[button.JOYSTICK1_LEFT] == HELD_DOWN:
				self.events[button.JOYSTICK1_LEFT] = UN_PRESSED		
		
		if self.device.axisValues[0] > self.sensitivity:
			if self.events[button.JOYSTICK1_RIGHT] == HELD_UP:
				self.events[button.JOYSTICK1_RIGHT] = PRESSED			
		else:
			if self.events[button.JOYSTICK1_RIGHT] == HELD_DOWN:
				self.events[button.JOYSTICK1_RIGHT] = UN_PRESSED				
		
		if self.device.axisValues[1] < -self.sensitivity:
			if self.events[button.JOYSTICK1_UP] == HELD_UP:
				self.events[button.JOYSTICK1_UP] = PRESSED			
		else:
			if self.events[button.JOYSTICK1_UP] == HELD_DOWN:
				self.events[button.JOYSTICK1_UP] = UN_PRESSED			
		
		if self.device.axisValues[1] > self.sensitivity:
			if self.events[button.JOYSTICK1_DOWN] == HELD_UP:
				self.events[button.JOYSTICK1_DOWN] = PRESSED			
		else:
			if self.events[button.JOYSTICK1_DOWN] == HELD_DOWN:
				self.events[button.JOYSTICK1_DOWN] = UN_PRESSED	







		if self.device.axisValues[4] < -self.sensitivity:
			if self.events[button.JOYSTICK2_LEFT] == HELD_UP:
				self.events[button.JOYSTICK2_LEFT] = PRESSED			
		else:
			if self.events[button.JOYSTICK2_LEFT] == HELD_DOWN:
				self.events[button.JOYSTICK2_LEFT] = UN_PRESSED		
		
		if self.device.axisValues[4] > self.sensitivity:
			if self.events[button.JOYSTICK2_RIGHT] == HELD_UP:
				self.events[button.JOYSTICK2_RIGHT] = PRESSED			
		else:
			if self.events[button.JOYSTICK2_RIGHT] == HELD_DOWN:
				self.events[button.JOYSTICK2_RIGHT] = UN_PRESSED				
		
		if self.device.axisValues[3] < -self.sensitivity:
			if self.events[button.JOYSTICK2_UP] == HELD_UP:
				self.events[button.JOYSTICK2_UP] = PRESSED			
		else:
			if self.events[button.JOYSTICK2_UP] == HELD_DOWN:
				self.events[button.JOYSTICK2_UP] = UN_PRESSED			
		
		if self.device.axisValues[3] > self.sensitivity:
			if self.events[button.JOYSTICK2_DOWN] == HELD_UP:
				self.events[button.JOYSTICK2_DOWN] = PRESSED			
		else:
			if self.events[button.JOYSTICK2_DOWN] == HELD_DOWN:
				self.events[button.JOYSTICK2_DOWN] = UN_PRESSED		

				
				
				
				
				
				

		if self.device.axisValues[2] > self.sensitivity:
			if self.events[button.LEFT_TRIGGER] == HELD_UP:
				self.events[button.LEFT_TRIGGER] = PRESSED			
		else:
			if self.events[button.LEFT_TRIGGER] == HELD_DOWN:
				self.events[button.LEFT_TRIGGER] = UN_PRESSED	

		if self.device.axisValues[2] < -self.sensitivity:
			if self.events[button.RIGHT_TRIGGER] == HELD_UP:
				self.events[button.RIGHT_TRIGGER] = PRESSED			
		else:
			if self.events[button.RIGHT_TRIGGER] == HELD_DOWN:
				self.events[button.RIGHT_TRIGGER] = UN_PRESSED		


				
				
				
				
				
				
		if self.device.hatValues[0] == 1:
			if self.events[button.ARROWS_UP] == HELD_UP:
				self.events[button.ARROWS_UP] = PRESSED			
	
		if self.device.hatValues[0] == 2:
			if self.events[button.ARROWS_RIGHT] == HELD_UP:
				self.events[button.ARROWS_RIGHT] = PRESSED			
	
		if self.device.hatValues[0] == 4:
			if self.events[button.ARROWS_DOWN] == HELD_UP:
				self.events[button.ARROWS_DOWN] = PRESSED			

		if self.device.hatValues[0] == 8:
			if self.events[button.ARROWS_LEFT] == HELD_UP:
				self.events[button.ARROWS_LEFT] = PRESSED			

		if self.device.hatValues[0] == 3:
			if self.events[button.ARROWS_UP] == HELD_UP:
				self.events[button.ARROWS_UP] = PRESSED		
			if self.events[button.ARROWS_RIGHT] == HELD_UP:
				self.events[button.ARROWS_RIGHT] = PRESSED	

		if self.device.hatValues[0] == 6:
			if self.events[button.ARROWS_DOWN] == HELD_UP:
				self.events[button.ARROWS_DOWN] = PRESSED		
			if self.events[button.ARROWS_RIGHT] == HELD_UP:
				self.events[button.ARROWS_RIGHT] = PRESSED	
				
		if self.device.hatValues[0] == 12:
			if self.events[button.ARROWS_DOWN] == HELD_UP:
				self.events[button.ARROWS_DOWN] = PRESSED		
			if self.events[button.ARROWS_LEFT] == HELD_UP:
				self.events[button.ARROWS_LEFT] = PRESSED

		if self.device.hatValues[0] == 9:
			if self.events[button.ARROWS_UP] == HELD_UP:
				self.events[button.ARROWS_UP] = PRESSED		
			if self.events[button.ARROWS_LEFT] == HELD_UP:
				self.events[button.ARROWS_LEFT] = PRESSED

		if self.device.hatValues[0] != 1 and self.device.hatValues[0] != 3 and self.device.hatValues[0] != 9:
			if self.events[button.ARROWS_UP] == HELD_DOWN:
				self.events[button.ARROWS_UP] = UN_PRESSED			
				
		if self.device.hatValues[0] != 2 and self.device.hatValues[0] != 3 and self.device.hatValues[0] != 6:
			if self.events[button.ARROWS_RIGHT] == HELD_DOWN:
				self.events[button.ARROWS_RIGHT] = UN_PRESSED					

		if self.device.hatValues[0] != 4 and self.device.hatValues[0] != 6 and self.device.hatValues[0] != 12:
			if self.events[button.ARROWS_DOWN] == HELD_DOWN:
				self.events[button.ARROWS_DOWN] = UN_PRESSED					

		if self.device.hatValues[0] != 8 and self.device.hatValues[0] != 12 and self.device.hatValues[0] != 9:
			if self.events[button.ARROWS_LEFT] == HELD_DOWN:
				self.events[button.ARROWS_LEFT] = UN_PRESSED	