# G-Suits Functions

# |Imports|

import bge
from gsuits_config import *
from gsuits_player import *
from gsuits_controller import *
import math
import mathutils

# |Definitions|

player = []
keyboard = bge.logic.keyboard
mouse = bge.logic.mouse
PRESSED = bge.logic.KX_INPUT_JUST_ACTIVATED
HELD_DOWN = bge.logic.KX_SENSOR_ACTIVE
HELD_UP = bge.logic.KX_SENSOR_INACTIVE
UN_PRESSED = bge.logic.KX_SENSOR_JUST_DEACTIVATED
camera = bge.logic.getCurrentScene().objects["main_camera"]

# |Free Functions|		

# |Player Set-up Functions|

def set_player_defaults(player):
	player.movement_speed = player_movement_speed
	player.spin_speed = player_spin_speed
	player.jump_speed = player_jump_speed
	player.allow_move_in_air = player_allow_move_in_air
	player.allow_double_jump = player_allow_double_jump
	player.spear_throw_speed = spear_throw_speed
	player.pickup_range = player_pickup_range
	player.projectile_correction_speed = projectile_correction_speed
	player.ground_sensor_length = player_ground_sensor_length
	player.crate_throw_speed = crate_throw_speed
	player.lives = player_lives
	
def set_player_controller_settings(player):
	player.input_move_left = player_move_left_cont
	player.input_move_right = player_move_right_cont
	player.input_jump = player_jump_cont
	player.input_crouch = player_crouch_cont
	player.input_pickup = player_pickup_cont
	player.input_throw = player_throw_cont
	if controller.is_active(bge.logic.globalDict["used_controllers"]) == True:
		player.controller = controller(bge.logic.globalDict["used_controllers"])
		bge.logic.globalDict["used_controllers"] += 1
		return True
	else:
		return False
	
	

def create_player1():
	try:
		bge.logic.getCurrentScene().objects[player1_spawn_name+bge.logic.globalDict["current_level"]]
	except:
		print("No spawn for player 1")
		return False
	else:
		new_player = astronaut_player(bge.logic.getCurrentScene().objects[player1_spawn_name+bge.logic.globalDict["current_level"]])
		
		# Config.
		if bge.logic.globalDict["player1_active"] == False:
			return False
		
		if bge.logic.globalDict["player1_cont"] == False:
			new_player.input_move_left = player1_move_left
			new_player.input_move_right = player1_move_right
			new_player.input_jump = player1_jump
			new_player.input_crouch = player1_crouch
			new_player.input_pickup = player1_pickup
			new_player.input_throw = player1_throw
		else:
			if set_player_controller_settings(new_player) == False:
				return False
		
		set_player_defaults(new_player)
		
		new_player.spawn()
		return new_player
	
def create_player2():
	try:
		bge.logic.getCurrentScene().objects[player2_spawn_name+bge.logic.globalDict["current_level"]]
	except:
		print("No spawn for player 2")
		return False
	else:
		new_player = astronaut_player(bge.logic.getCurrentScene().objects[player2_spawn_name+bge.logic.globalDict["current_level"]])
		
		# Config.
		if bge.logic.globalDict["player2_active"] == False:
			return False
		
		if bge.logic.globalDict["player2_cont"] == False:
			new_player.input_move_left = player2_move_left
			new_player.input_move_right = player2_move_right
			new_player.input_jump = player2_jump
			new_player.input_crouch = player2_crouch
			new_player.input_pickup = player2_pickup
			new_player.input_throw = player2_throw
		else:
			if set_player_controller_settings(new_player) == False:
				return False
		
		set_player_defaults(new_player)
		
		new_player.spawn()
		return new_player
		
def create_player3():
	try:
		bge.logic.getCurrentScene().objects[player3_spawn_name+bge.logic.globalDict["current_level"]]
	except:
		print("No spawn for player 3")
		return False
	else:
		new_player = astronaut_player(bge.logic.getCurrentScene().objects[player3_spawn_name+bge.logic.globalDict["current_level"]])
		
		# Config.
		if bge.logic.globalDict["player3_active"] == False:
			return False
		
		if bge.logic.globalDict["player3_cont"] == False:
			new_player.input_move_left = player3_move_left
			new_player.input_move_right = player3_move_right
			new_player.input_jump = player3_jump
			new_player.input_crouch = player3_crouch
			new_player.input_pickup = player3_pickup
			new_player.input_throw = player3_throw
		else:
			if set_player_controller_settings(new_player) == False:
				return False
		
		set_player_defaults(new_player)
		
		new_player.spawn()
		return new_player	

def create_player4():
	try:
		bge.logic.getCurrentScene().objects[player4_spawn_name+bge.logic.globalDict["current_level"]]
	except:
		print("No spawn for player 4")
		return False
	else:
		new_player = astronaut_player(bge.logic.getCurrentScene().objects[player4_spawn_name+bge.logic.globalDict["current_level"]])
		
		# Config.
		if bge.logic.globalDict["player4_active"] == False:
			return False
		
		if bge.logic.globalDict["player4_cont"] == False:
			new_player.input_move_left = player4_move_left
			new_player.input_move_right = player4_move_right
			new_player.input_jump = player4_jump
			new_player.input_crouch = player4_crouch
			new_player.input_pickup = player4_pickup
			new_player.input_throw = player4_throw
		else:
			if set_player_controller_settings(new_player) == False:
				return False
		
		set_player_defaults(new_player)
		
		new_player.spawn()
		return new_player			

def spawn_players():
	player = []
	bge.logic.globalDict["used_controllers"] = 0
	
	player1 = create_player1()
	if player1 != False:
		player.append(player1)
		
	player2 = create_player2()
	if player2 != False:
		player.append(player2)
		
	player3 = create_player3()
	if player3 != False:
		player.append(player3)	

	player4 = create_player4()
	if player4 != False:
		player.append(player4)			
	
	bge.logic.globalDict["player"] = player	
	
	return player
