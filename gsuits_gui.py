# G-Suits GUI

# |Imports|

import bge
import math
import mathutils
import aud


from gsuits_config import *

# |Main|
def main():

	# |Definitions|

	keyboard = bge.logic.keyboard
	mouse = bge.logic.mouse
	PRESSED = bge.logic.KX_INPUT_JUST_ACTIVATED
	HELD_DOWN = bge.logic.KX_SENSOR_ACTIVE
	HELD_UP = bge.logic.KX_SENSOR_INACTIVE
	UN_PRESSED = bge.logic.KX_SENSOR_JUST_DEACTIVATED


	# |Startup|
	# (Runs once)

	def startup():
		print("GUI script running...")
		bge.logic.globalDict["startup_gui"] = True
		
		first_camera = bge.logic.getCurrentScene().objects["gui_3"]
		
		try:
			bge.logic.globalDict["player_wins"]

			# Set up the sound		
			device = aud.device()
			# load sound file (it can be a video file with audio)
			factory = aud.Factory('Sounds/Man Dying.wav')
			# set volume
			factory = factory.volume(.1)
			# play the audio, this return a handle to control play/pause
			handle = device.play(factory)
			
			bge.logic.getCurrentScene().objects["text_player_wins"].text = "PLAYER "+str(bge.logic.globalDict["player_wins"])+" WINS!"
			
		except KeyError:
			first_camera = bge.logic.getCurrentScene().objects["gui_1"]
		
		
		# Setting up camera...
		bge.logic.getCurrentScene().active_camera = first_camera
		
		# Setting up mouse...
		bge.render.showMouse(True)	
		
		for obj in bge.logic.getCurrentScene().objects:
			if "select_activate_player" in obj.name or "select_cont_player" in obj.name:
				obj.color = [255, 0, 0, 1.0]
				
				
		# Initializing menu state...
		bge.logic.globalDict["player1_active"] = False
		bge.logic.globalDict["player2_active"] = False
		bge.logic.globalDict["player3_active"] = False
		bge.logic.globalDict["player4_active"] = False
		
		bge.logic.globalDict["player1_cont"] = False
		bge.logic.globalDict["player2_cont"] = False
		bge.logic.globalDict["player3_cont"] = False
		bge.logic.globalDict["player4_cont"] = False		
				
				
		
		
	# Runs startup if bge.logic.globalDict["startup"] is not defined.
	try:
		bge.logic.globalDict["startup_gui"]
	except KeyError:
		startup()
		
		
	# |Post Startup Redefinitions|
	camera = bge.logic.getCurrentScene().active_camera
		

	# |Pulse|
	# (Runs once per render)

	def pulse():
		
		# Button pressed events.
		if mouse.events[bge.events.LEFTMOUSE] == PRESSED:
			obj = camera.getScreenRay(mouse.position[0],mouse.position[1],1000)
			if obj != None:
				if "select_activate_player" in obj.name:
					if obj.color[0] == 255:
						obj.color = [0, 255, 0, 0.5]
						if "1" in obj.name:
							bge.logic.globalDict["player1_active"] = True
						if "2" in obj.name:
							bge.logic.globalDict["player2_active"] = True
						if "3" in obj.name:
							bge.logic.globalDict["player3_active"] = True
						if "4" in obj.name:
							bge.logic.globalDict["player4_active"] = True					
					else:
						obj.color = [255, 0, 0, 0.3]
						if "1" in obj.name:
							bge.logic.globalDict["player1_active"] = False
						if "2" in obj.name:
							bge.logic.globalDict["player2_active"] = False
						if "3" in obj.name:
							bge.logic.globalDict["player3_active"] = False
						if "4" in obj.name:
							bge.logic.globalDict["player4_active"] = False							
				
				if "select_cont_player" in obj.name:
					if obj.color[0] == 255:
						obj.color = [0, 255, 0, 1.0]
						if "1" in obj.name:
							bge.logic.globalDict["player1_cont"] = True
						if "2" in obj.name:
							bge.logic.globalDict["player2_cont"] = True
						if "3" in obj.name:
							bge.logic.globalDict["player3_cont"] = True
						if "4" in obj.name:
							bge.logic.globalDict["player4_cont"] = True							
					else:
						obj.color = [255, 0, 0, 1.0]
						if "1" in obj.name:
							bge.logic.globalDict["player1_cont"] = False
						if "2" in obj.name:
							bge.logic.globalDict["player2_cont"] = False
						if "3" in obj.name:
							bge.logic.globalDict["player3_cont"] = False
						if "4" in obj.name:
							bge.logic.globalDict["player4_cont"] = False								
				
				if "button_select_level" in obj.name:
					bge.logic.getCurrentScene().active_camera = bge.logic.getCurrentScene().objects["gui_2"]
			
				if "button_continue" in obj.name:
					bge.logic.getCurrentScene().active_camera = bge.logic.getCurrentScene().objects["gui_1"]				
			
				if "button_mining_level" in obj.name:
					bge.logic.globalDict["current_level"] = mining_level
					bge.logic.getCurrentScene().replace("levels")
				elif "button_orbital_level" in obj.name:
					bge.logic.globalDict["current_level"] = orbital_level
					bge.logic.getCurrentScene().replace("levels")
				elif "button_colony_level" in obj.name:
					bge.logic.globalDict["current_level"] = colony_level		
					bge.logic.getCurrentScene().replace("levels")
		
	pulse()
		
		
		