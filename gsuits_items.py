# G-Suits Item Class

# |Imports|

import bge
import math
import mathutils
from gsuits_config import *

# |Class Declaration|

class item_container:
	
	def __init__(self):
		self.items = []
	
	def add(self, object_type):
		self.items.append(object_type)
	
	# @return TRUE if the objects name is an item name.
	def is_item(self, object_name):
		for item in self.items:
			if item == object_name:
				return True
			if item == object_name[:-4]:
				# cuts off the .001 whatever
				return True
		return False
		
	def get_type(self, object_name):
		if "." in object_name:
			return object_name[:-4]
		else:
			return object_name