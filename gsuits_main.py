# G-Suits Entry Point
# Author: Evan Baad

# |Imports|

import bge
from gsuits_config import *
from gsuits_player import *
from gsuits_functions import *
from gsuits_projectile import *
from gsuits_camera import *
from gsuits_items import *
import math
import mathutils



# |Main|
def main():

	# |Definitions|

	data = bge.logic.globalDict;
	object = bge.logic.getCurrentScene().objects;
	keyboard = bge.logic.keyboard
	mouse = bge.logic.mouse
	PRESSED = bge.logic.KX_INPUT_JUST_ACTIVATED
	HELD_DOWN = bge.logic.KX_SENSOR_ACTIVE
	HELD_UP = bge.logic.KX_SENSOR_INACTIVE
	UN_PRESSED = bge.logic.KX_SENSOR_JUST_DEACTIVATED

	# |Startup|
	# (Runs once)

	def startup():
		print("Start-up script running...")
		data["startup"] = True
		
		
		
		# Spawning players...
		player =spawn_players()
		
			
		# Setting up camera...
		camera = camera_controller(camera_name, camera_start_distance, camera_angle)
		camera.zoom_in_fov = zoom_in_fov
		camera.zoom_out_fov = zoom_out_fov
		camera.zoom_min = zoom_min
		camera.zoom_max = zoom_max
		camera.zoom_in_speed = zoom_in_speed
		camera.zoom_out_speed = zoom_out_speed
		
		data["camera"] = camera
		
		# Setting up item container...
		items = item_container()
		items.add("spear")
		items.add("steel_crate")
		
		data["items"] = items
		
		for p in player:
			p.items = items
			
		
		# Setting up mouse...
		bge.render.showMouse(False)
		
	# Runs startup if data["startup"] is not defined.
	try:
		data["startup"]
	except KeyError:
		startup()


		
	# |Post Startup Redefinitions|
	player = data["player"]
	camera = data["camera"]
	items = data["items"]

	# |Pulse|
	# (Runs once per render)

	def pulse():
		
		# Updating players...
		players_alive = 0
		for p in player:
			p.update()
			
			# Locking to x axis
			if p.main_object.position.x > 0.2 or p.main_object.position.x < -0.2:
				p.main_object.position.x = 0
				p.main_object.worldLinearVelocity.x = 0
				

				
		# Player wins.
		players_with_lives = 0
		for p in player:
			if p.lives > 0:
				players_with_lives += 1
		
		if players_with_lives <= 1:
			index = 0
			for p in player:
				index += 1
				if p.lives > 0:
					bge.logic.globalDict["player_wins"] = index
			data.pop("startup_gui", None)
			data.pop("startup", None)
			bge.logic.getCurrentScene().replace("gui")
		
				
		# Updating items...
		#for obj in object:
		#	if items.is_item(obj.name):
		#		if obj.position.x > 0.2 or obj.position.x < -0.2:
		#			obj.position.x = 0
		#			obj.worldLinearVelocity.x = 0			
			
		# Updating camera...
		camera.update(player)
		
	pulse()



