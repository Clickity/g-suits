# G-Suits Player Class

# |Imports|

import bge
import math
import mathutils
from gsuits_projectile import *
from gsuits_items import *
from gsuits_controller import *

# |Definitions|
keyboard = bge.logic.keyboard
mouse = bge.logic.mouse
PRESSED = bge.logic.KX_INPUT_JUST_ACTIVATED
HELD_DOWN = bge.logic.KX_SENSOR_ACTIVE
HELD_UP = bge.logic.KX_SENSOR_INACTIVE
UN_PRESSED = bge.logic.KX_SENSOR_JUST_DEACTIVATED


# |Class Definition|

class astronaut_player:
	
	@staticmethod
	def get_inst(player_obj):
		players = bge.logic.globalDict["player"]
		for p in players:
			if p.main_object == player_obj:
				return p
		return False
	
	
	# Constructor
	# Note: In python, member variables are declared
	# inside the constructor with the self.<variable_name>
	# syntax.
	# @param spawn_point the position/orientation the player
	# spawns at/with.
	def __init__(self, spawn_point):
		print("Player created.")
		
		
		# |Configuration Variables|
		self.movement_speed = 0.1
		self.spin_speed = 10.0
		self.jump_speed = 1000
		self.allow_move_in_air = True
		self.allow_double_jump = True
		self.items = item_container()
		self.spear_throw_speed = 30.0
		self.crate_throw_speed = 10.0
		self.pickup_range = 7.5		
		self.projectile_correction_speed = 1.0
		self.ground_sensor_length = 0.4
		self.spawn_point = spawn_point
		self.lives = 3
		
		# |Input Configuration Variables|
		# These members will manually configure which
		# button activates an action.
		# Default of -1 means that there is no input.
		# Possible values can be found at:
		# http://www.blender.org/documentation/blender_python_api_2_69_1/bge.events.html#keyboard-keys
		self.input_move_left = -1
		self.input_move_right = -1
		self.input_jump = -1
		self.input_crouch = -1
		self.input_pickup = -1
		self.input_throw = -1
		self.input_kick = -1
		
		self.controller = None
		
		
		# |Member Initialization|
		
		self.projectiles = []
		self.is_moving_left = False
		self.is_moving_right = False
		self.is_on_ground = False
		self.is_spinning_left = False
		self.is_spinning_right = False
		self.can_double_jump = False
		self.is_crouching = False
		self.prev_anim_loop = ""
		self.prev_end_blendin = 0.0
		self.weapon = None
		self.is_aiming = False
		self.is_throwing = False
		self.is_throwing_twohand = False
		self.is_kicking = False
		self.is_hauling_item = False
		self.is_dying = False
		self.is_dead = False
		
		
	# Causes the player to update its state based
	# on the state of the game.
	# Things that are updated:
	# - Input
	# - Position
	# - Animations
	# - Certain collisions (ex. ground collisions)
	def update(self):
		
		# |Updating States|
		
		# Controller.
		if self.controller != None:
			self.controller.update()
		
		# On ground.
		# Note: placeholders are there to make the function return a 3-tuple.
		pos = self.sensor_ground_left.worldPosition
		hit_left = self.sensor_ground_left.rayCastTo([pos.x,pos.y,pos.z-self.ground_sensor_length])
		pos = self.sensor_ground_right.worldPosition
		hit_right = self.sensor_ground_right.rayCastTo([pos.x,pos.y,pos.z-self.ground_sensor_length])
		if hit_left != None or hit_right != None:
			self.is_on_ground = True
			self.can_double_jump = True
		else:
			self.is_on_ground = False		

			
		# Jumping
		if self.input_jump != -1:
			if (self.controller == None and keyboard.events[self.input_jump] == PRESSED) or (self.controller != None and self.controller.events[self.input_jump] == PRESSED):
				if self.is_on_ground == True:
					self.main_object.applyForce([0.0, 0.0, self.jump_speed], False)
				elif self.can_double_jump == True and self.allow_double_jump:
					self.can_double_jump = False
					self.main_object.applyForce([0.0, 0.0, self.jump_speed], False)	

			
		# Crouching.
		
		if self.input_crouch != -1:
			if ((self.controller == None and keyboard.events[self.input_crouch] == HELD_DOWN) or (self.controller != None and self.controller.events[self.input_crouch] == HELD_DOWN)) and self.is_on_ground == True:
				self.is_crouching = True
			else:
				self.is_crouching = False			
			
			
		# Moving left.
		if self.input_move_left != -1:
			if (self.controller == None and keyboard.events[self.input_move_left] == HELD_DOWN) or (self.controller != None and self.controller.events[self.input_move_left] == HELD_DOWN):
				if self.is_on_ground == True or self.allow_move_in_air == True:
					self.is_moving_left = True
			if (self.controller == None and keyboard.events[self.input_move_left] == HELD_UP) or (self.controller != None and self.controller.events[self.input_move_left] == HELD_UP):
				if self.is_on_ground == True or self.allow_move_in_air == True:
					self.is_moving_left = False
		
		if self.is_moving_left == True and self.is_crouching == False and self.is_moving_right == False:
			self.main_object.applyMovement([0.0, -self.movement_speed, 0.0], False)
			rot = self.main_object.worldTransform.to_euler("XYZ")[2]
			if rot != math.radians(-180):
				self.is_spinning_left = True

		if self.is_spinning_left == True:
			rot = self.main_object.worldTransform.to_euler("XYZ")[2]
			self.is_spinning_right = False
			if (rot + math.radians(-self.spin_speed)) > math.radians(-180) and rot != math.radians(180):
				self.main_object.applyRotation([0.0, 0.0, math.radians(-self.spin_speed)],False)
			else:
				self.is_spinning_left = False
				self.main_object.worldOrientation[1][1] = -1				
		
		# Moving right.
		if self.input_move_right != -1:
			if (self.controller == None and keyboard.events[self.input_move_right] == HELD_DOWN) or (self.controller != None and self.controller.events[self.input_move_right] == HELD_DOWN):
				if self.is_on_ground == True or self.allow_move_in_air == True:
					self.is_moving_right = True
			if (self.controller == None and keyboard.events[self.input_move_right] == HELD_UP) or (self.controller != None and self.controller.events[self.input_move_right] == HELD_UP):
				if self.is_on_ground == True or self.allow_move_in_air == True:
					self.is_moving_right = False
		
		if self.is_moving_right == True and self.is_crouching == False:
			self.main_object.applyMovement([0.0, self.movement_speed, 0.0], False)
			rot = self.main_object.worldTransform.to_euler("XYZ")[2]
			if rot != math.radians(-0):
				self.is_spinning_right = True		
		
		if self.is_spinning_right == True:
			rot = self.main_object.worldTransform.to_euler("XYZ")[2]
			self.is_spinning_left = False
			if rot < math.radians(0.0):
				self.main_object.applyRotation([0.0, 0.0, math.radians(self.spin_speed)],False)
			else:
				self.is_spinning_right = False
				self.main_object.worldOrientation[1][1] = 1		
		
		
		# Picking up weapon.
		
		if self.input_pickup != -1:
			if (self.controller == None and keyboard.events[self.input_pickup] == PRESSED) or (self.controller != None and self.controller.events[self.input_pickup] == PRESSED):
				for obj in bge.logic.getCurrentScene().objects:
					if self.items.is_item(obj.name):
						if math.sqrt((obj.worldPosition[0]-self.main_object.worldPosition[0])**2+(obj.worldPosition[1]-self.main_object.worldPosition[1])**2+(obj.worldPosition[2]-self.main_object.worldPosition[2])**2) <= self.pickup_range:
							print("Picking up:" + obj.name)
							
							if self.items.get_type(obj.name) == "spear":
								self.weapon = obj
								self.weapon.removeParent()
								self.weapon.worldPosition = self.spawn_weapon.worldPosition
								self.weapon.worldOrientation = self.spawn_weapon.worldOrientation
								self.weapon.restoreDynamics()
								self.weapon.setParent(self.spawn_weapon, False, True)

							
							if self.items.get_type(obj.name) == "steel_crate":
								self.is_hauling_item = True
								self.weapon = obj
								self.weapon.worldPosition = self.spawn_carry_item.worldPosition
								self.weapon.worldOrientation = self.spawn_carry_item.worldOrientation
								self.weapon.worldPosition.z += 2.5
								self.weapon.restoreDynamics()
								self.weapon.setParent(self.spawn_carry_item, False, False)
							
							break
				
		
		# Throwing weapon.
		
		self.is_throwing = False
		self.is_throwing_twohand = False
		if self.input_throw != -1 and self.weapon != None:
			if (self.controller == None and keyboard.events[self.input_throw] == HELD_DOWN) or (self.controller != None and self.controller.events[self.input_throw] == HELD_DOWN):
				if self.is_hauling_item == False:
					self.is_aiming = True
			
			if ((self.controller == None and keyboard.events[self.input_throw] == UN_PRESSED) or (self.controller != None and self.controller.events[self.input_throw] == UN_PRESSED)) and self.is_spinning_left == False and self.is_spinning_right == False and self.weapon != None: 
				if self.is_hauling_item == False:
					self.is_aiming = False
					self.is_throwing = True
				else:
					self.is_hauling_item = False
					self.is_throwing_twohand = True
				
				if self.items.get_type(self.weapon.name) == "spear":
					print("Throwing spear.")
					self.weapon.removeParent()
					p = projectile(self.weapon)
					self.projectiles.append(p)
					if self.main_object.worldOrientation[1][1] == -1:
						p.launch(self.spear_throw_speed+math.fabs(self.main_object.worldLinearVelocity.x), 180, 0.0, lambda hit: self.collision(hit, p))
					elif self.main_object.worldOrientation[1][1] == 1:
						p.launch(self.spear_throw_speed+math.fabs(self.main_object.worldLinearVelocity.x), 0, 0.0, lambda hit: self.collision(hit, p))
					self.weapon = None
					
				elif self.items.get_type(self.weapon.name) == "steel_crate":
					print("Throwing crate.")
					self.weapon.removeParent()
					p = projectile(self.weapon)
					self.projectiles.append(p)
					if self.main_object.worldOrientation[1][1] == -1:
						p.launch(self.crate_throw_speed+math.fabs(self.main_object.worldLinearVelocity.x), 180, 0.0, lambda hit: self.collision(hit, p))
					elif self.main_object.worldOrientation[1][1] == 1:
						p.launch(self.crate_throw_speed+math.fabs(self.main_object.worldLinearVelocity.x), 0, 0.0, lambda hit: self.collision(hit, p))
					self.weapon = None
					
		# Kick.
		
		self.is_kicking = False
		if self.input_kick != -1:
			if (self.controller == None and keyboard.events[self.input_kick] == PRESSED) or (self.controller != None and self.controller.events[self.input_kick] == PRESSED):
				self.is_kicking = True
				
		
		# Updating projectiles.
		for p in self.projectiles:
			if p.main_object.worldPosition.x > 0:
				p.main_object.worldLinearVelocity.x = -self.projectile_correction_speed
			elif p.main_object.worldPosition.x < 0:
				p.main_object.worldLinearVelocity.x = self.projectile_correction_speed	
			
			if math.fabs(p.main_object.worldPosition.x) < 0.2:
				p.main_object.worldLinearVelocity.x = 0
			
			if 0.2 > math.fabs(p.main_object.worldLinearVelocity.x+p.main_object.worldLinearVelocity.y+p.main_object.worldLinearVelocity.z+p.main_object.worldAngularVelocity.x+p.main_object.worldAngularVelocity.y+p.main_object.worldAngularVelocity.z):
				print("0 remove")
				self.projectiles.remove(p)
				
		# |Updating Animations|
			
		
		# Crouch.
		if self.is_on_ground == True and self.is_crouching == True:
			self.loop_animation("crouch", 0.0, 80.0, 1, 1, 5.0, 10.0)

		# Idle.
		if self.is_on_ground == True and self.is_moving_left == False and self.is_moving_right == False and self.is_crouching == False:
			#bones.playAction("Idle", 0.0, 66.0, 1, 0, 10.0)
			self.loop_animation("Idle", 0.0, 66.0, 1, 1, 10.0, 0.0)
			
		# Walk.
		if self.is_on_ground == True and (self.is_moving_left == True or self.is_moving_right == True) and self.is_crouching == False:
			self.loop_animation("Walk Cycle", 0.4, 28.6, 1, 1, 5.0, 0.0)

		# Jump.
		if self.is_on_ground == False:
			self.loop_animation("jump", 0.0, 68.0, 1, 1, 5.0, 0.0)	

		# Throw.
		if self.is_aiming == True and self.is_hauling_item == False:
			self.loop_animation("arm back", 0.0, 10.0, 2, 1, 5.0, 0.0)

		if self.is_throwing == True:
			self.loop_animation("arm release", 0.0, 15.0, 2, 1, 7.0, 0.0)
			
		if self.is_throwing_twohand == True:
			self.loop_animation("throw_large", 0.0, 40.0, 2, 1, 40.0, 20.0, 20.0)
	
		# Kick.
		
		if self.is_kicking == True:
			self.loop_animation("kick", 0.0, 15.0, 2, 1, 5.0, 0.0)		
		
		# Haul bge.logic.getCurrentScene().objects.
		
		if self.is_hauling_item == True:
			self.loop_animation("hold_large", 0.0, 43.0, 2, 1, 2.0, 0.0)
		
		# 1 frame = grab_large, throw_large,
		# 30 frame = pickup large
	
	# Used for animations that need to be looped to look smooth.
	# @param name animation name.
	# @param start the start time for the animation.
	# @param finish the finish time for the animation.
	# @param layer the layter the animation runs in.
	# @param priority lower priority means that the animation will
	# run over an animation with higher priority.
	# @param start_blendin the time to spend smoothing out transitions between
	# different animations. This is only relevant when the animation is
	# chaning to a different one.
	# @param loop_blendin the time to spend smoothing out transitions to the next
	# loop of the same animation.
	# @param end_blendin the blendin time to be added to the next actions start_blendin time.
	def loop_animation(self, name, start, finish, layer = 0, priority = 0, start_blendin = 0.0, loop_blendin = 0.0, end_blendin = 0.0):
		if name == self.prev_anim_loop:
			self.animation_object.playAction(name, start, finish, layer, priority, loop_blendin)
		else:
			self.animation_object.playAction(name, start, finish, layer, priority, start_blendin + self.prev_end_blendin)
		
		self.prev_end_blendin = end_blendin
		self.prev_anim_loop = name
		
	
	# Callback function to call after a projectile collision.
	# @param hit the hit bge.logic.getCurrentScene().objects.
	# @param projectile the projectile that hit the bge.logic.getCurrentScene().objects.
	def collision(self, hit, projectile):
		print("Collision with " + hit.name)
		
		if self.items.get_type(projectile.main_object.name) == "spear":
			
			# Spear hit.
			projectile.main_object.worldLinearVelocity = [0,0,0]
			projectile.main_object.worldAngularVelocity = [0,0,0]
			projectile.main_object.setParent(hit, False, True)
			projectile.main_object.suspendDynamics()		
			self.projectiles.remove(projectile)			
			
			
			# Spear hit parent, so pick it up.
			parent_obj = self.get_obj_max_parent(hit)
			
			if parent_obj == self.main_object:
				self.weapon = projectile.main_object
				self.weapon.removeParent()
				self.weapon.worldPosition = self.spawn_weapon.worldPosition
				self.weapon.worldOrientation = self.spawn_weapon.worldOrientation
				self.weapon.restoreDynamics()
				self.weapon.setParent(self.spawn_weapon, False, True)	
			else:
				# Hit another astronaut.
				if parent_obj.name == "astronaut":
					hit_player = astronaut_player.get_inst(parent_obj)
				
					# Spear hit enemy helmet.
					if hit.name == "Helmet":
						print("Helmet shatter begin...")
						hit_player.shatter_helmet()
						projectile.main_object.removeParent()
						projectile.main_object.restoreDynamics()				
					
					hit_player.respawn()
					
					
	def spawn(self):
		# |Spawning Player|
		
		
		# Parent bge.logic.getCurrentScene().objects to the astronaut.
		self.main_object = bge.logic.getCurrentScene().addObject("astronaut", self.spawn_point)
		
		# Bones that animate the astronaut.
		self.animation_object = self.main_object.children["Bones"]
		
		# Creating sensors...
		
		# Ground sensors.
		self.sensor_ground_left = self.main_object.children["sensor_ground_left"]
		self.sensor_ground_right = self.main_object.children["sensor_ground_right"]
		
		
		# Creating spawn points...
		
		self.spawn_weapon = self.main_object.childrenRecursive["spawn_weapon"]
		self.spawn_carry_item = self.main_object.childrenRecursive["spawn_carry_item"]	
		self.spawn_helmet_shatter = self.main_object.childrenRecursive["Helmet"]
			
			
	def shatter_helmet(self):
		for i in range(8):
			self.spawn_helmet_shatter.visible = False
			self.spawn_helmet_shatter.occlusion = False
			shard = bge.logic.getCurrentScene().addObject("HelmetShatter.00%i"%i, self.spawn_helmet_shatter)
			if i == 0:
				shard.setParent(self.spawn_helmet_shatter, False, True)
			
		
	def get_obj_max_parent(self, obj):
		max_parent = obj
		while max_parent.parent != None:
			max_parent = max_parent.parent
		return max_parent
	
	def respawn(self):
		#self.is_dead = True
		self.main_object.endObject()
		if self.lives > 0:
			self.lives -= 1
			self.spawn()
		
	