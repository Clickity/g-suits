# G-Suits Projectile Class

# |Imports|

import bge
import math
import mathutils
from gsuits_config import *

# |Definitions|

data = bge.logic.globalDict;
object = bge.logic.getCurrentScene().objects;


# |Class Definition|

class projectile:
	
	# Constructor
	# @param projectile_type a string that identifies the type of projectile
	# for collisions.
	# @param projectile_name the object name of the projectile.
	# @param projectile_spawn_point the place to spawn the projectile or the place to move it.
	# @param copy whether to copy the object given and make a new one or to use the existing object.
	def __init__(self, projectile_object, projectile_spawn_point = False, copy = True):
		print("Projectile created.")
		
		# |Projectile Properties|
		# - Angle
		# - Speed
		# - Game object
		# - Type
		# - Spin
		
		# |Spawning Projectile|
		
		if copy == True and projectile_spawn_point != False:
			self.main_object = bge.logic.getCurrentScene().addObject(projectile_object, projectile_spawn_point)
		else:
			if isinstance(projectile_object, str):
				self.main_object = object[projectile_object]
			else:
				self.main_object = projectile_object
			
			if projectile_spawn_point != False:
				self.main_object.worldPosition = object[projectile_spawn_point].worldPosition
				self.main_object.worldOrientation = object[projectile_spawn_point].worldOrientation
		
		# |Member Initialization|
		
		self.speed = 0
		self.angle = 0
		self.spin = 0
		self.collision_cb = None
		
	# Launches the projectile.
	# @param speed the speed of the projectile.
	# @param angle the angle of the projectile. 0 is +Y, 180 is -Y.
	# @param spin the spin speed of the projectile.
	def launch(self, speed, angle, spin, collision_cb=None):
		self.speed = speed
		self.angle = angle
		self.spin = spin
		self.collision_cb = collision_cb
		
		self.main_object.restoreDynamics()
		self.main_object.alignAxisToVect([0.0, math.cos(math.radians(self.angle)), math.sin(math.radians(self.angle))], 1, 1.0)
		self.main_object.setLinearVelocity([0.0, self.speed * math.cos(math.radians(self.angle)), self.speed * math.sin(math.radians(self.angle))],False)
		self.main_object.setAngularVelocity([spin, 0.0, 0.0], False)
		
		self.main_object.collisionCallbacks.append(lambda hit: self.collision(hit))
		
	def collision(self, hit):
		self.main_object.collisionCallbacks.pop()
		if self.collision_cb != None:
			self.collision_cb(hit)
		
		
	
	
	
	
	
	
	
		