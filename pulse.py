
# Evan's Code For 2 Pro Bros

import bge
import math
import mathutils
from bge import logic

# |REQUIRED|

# - Astronaut object named "astronaut".
# - Camera named "main_camera".
# - Cube named "sensor_ground" parented to and at the bottom
#   of astronaut.
# - Empty called "spawn_spear" at the spawn point of the
#   spear and parented to the astronaut.
# - Spear to spawn in another layer named "thrown_spear".

# |CONFIG|

# Note: In general for toggles 1 is on, 0 is off.

# Astronaut movement speed.
move_speed = 0.1

# Astronaut black man density.
jump_speed = 1000

# Enables double jump for the astronaut.
enable_double_jump = 1

# If enabled, the astronaut can only control
# his horizontal movement when he is on the ground.
enable_move_on_ground = 0

# Enables this outdated sensor for detecting if
# the astronaut is on the ground.
enable_velocity_ground_sensor = 0

# Sets the rate that the astronaut turns at.
spin_speed = 10.0;

# The amount of ticks that the spear will exist for.
spear_lifetime = 500

# The speed the spear moves at when thrown.
spear_speed = 0.5

# Adds a spin the spear so it curves down
# when thrown.
spear_gravity_spin = 0.005

# Sets the distance that the astronaut has to be from the cameras center
# for it to relocate. Tip: Set it so that the camera changes at the edge
# of the screen.
camera_scene_width = 20

# If enabled, the camera will constantly center on the astronaut.
enable_constant_camera_tracking = 0


# |INIT|

# objects container
objects = logic.getCurrentScene().objects

# globaldict container
dict = bge.logic.globalDict

# projectile object container
projectiles = dict["projectiles"]

# |GETTING OBJECTS|
astronaut = objects["astronaut"];
bones = objects["Bones"];
sensor_ground = None
if enable_velocity_ground_sensor != 1:
	sensor_ground = objects["sensor_ground"];


# |IS ASTRONAUT ON GROUND|

if enable_velocity_ground_sensor == 1:
	if astronaut.getLinearVelocity(False)[2] < 0.1 and astronaut.getLinearVelocity(False)[2] >= 0.0:
		dict["on_ground"] = 1
		dict["double_jump"] = 1
	else:
		dict["on_ground"] = 2
else:
	obj, hitpoint, hitnormal = astronaut.rayCast(sensor_ground, None)
	if obj != None:
		dict["on_ground"] = 1
		dict["double_jump"] = 1
	else:
		dict["on_ground"] = 2		


# |INPUT|	

# The all keys thing without a keyboard sensor (but you will
# need an always sensor with pulse mode on)
keyboard = bge.logic.keyboard
mouse = bge.logic.mouse

PRESSED = bge.logic.KX_INPUT_JUST_ACTIVATED
HELD_DOWN = bge.logic.KX_SENSOR_ACTIVE
HELD_UP = bge.logic.KX_SENSOR_INACTIVE
UN_PRESSED = bge.logic.KX_SENSOR_JUST_DEACTIVATED

# JUMP
if keyboard.events[bge.events.WKEY] == PRESSED:
	if dict["on_ground"] == 1:
		astronaut.applyForce([0.0, 0.0, jump_speed], False)
		#astronaut.applyMovement([0.0, 0.0, jump_speed], False)
	elif dict["double_jump"] == 1 and enable_double_jump == 1:
		dict["double_jump"] = 2
		astronaut.applyForce([0.0, 0.0, jump_speed], False)
	
# CROUCH
is_crouched = 2
if keyboard.events[bge.events.SKEY] == HELD_DOWN:
	is_crouched = 1
	
# MOVE LEFT
if keyboard.events[bge.events.AKEY] == HELD_DOWN:
	if dict["on_ground"] == 1 or enable_move_on_ground == 0:
		dict["moving_left"] = 1
if keyboard.events[bge.events.AKEY] == HELD_UP:
	if dict["on_ground"] == 1 or enable_move_on_ground == 0:
		dict["moving_left"] = 2

if dict["moving_left"] == 1 and is_crouched == 2:
	astronaut.applyMovement([0.0, -move_speed, 0.0], False)
	rot = astronaut.worldTransform.to_euler("XYZ")[2]
	if rot != math.radians(-180):
		dict["spin_left"] = 1

if dict["spin_left"] == 1:
	rot = astronaut.worldTransform.to_euler("XYZ")[2]
	
	if dict["spin_right"] == 1:
		dict["spin_right"] = 2
	
	if (rot + math.radians(-spin_speed)) > math.radians(-180) and rot != math.radians(180):
		astronaut.applyRotation([0.0, 0.0, math.radians(-spin_speed)],False)
	else:
		dict["spin_left"] = 2
		astronaut.worldOrientation[1][1] = -1
	
# MOVE RIGHT
if keyboard.events[bge.events.DKEY] == HELD_DOWN:
	if dict["on_ground"] == 1 or enable_move_on_ground == 0:
		dict["moving_right"] = 1
if keyboard.events[bge.events.DKEY] == HELD_UP:
	if dict["on_ground"] == 1 or enable_move_on_ground == 0:
		dict["moving_right"] = 2

if dict["moving_right"] == 1 and is_crouched == 2:
	astronaut.applyMovement([0.0, move_speed, 0.0], False)
	rot = astronaut.worldTransform.to_euler("XYZ")[2]
	if rot != math.radians(-0):
		dict["spin_right"] = 1

if dict["spin_right"] == 1:
	rot = astronaut.worldTransform.to_euler("XYZ")[2]
	
	if dict["spin_left"] == 1:
		dict["spin_left"] = 2
	
	if rot < math.radians(0.0):
		astronaut.applyRotation([0.0, 0.0, math.radians(spin_speed)],False)
	else:
		dict["spin_right"] = 2
		astronaut.worldOrientation[1][1] = 1
		
# throwing
def be_a_man(hitter, hit):
	hitter[2] = 2
	hitter[0].suspendDynamics()

is_throwing = 2
if mouse.events[bge.events.LEFTMOUSE] == HELD_DOWN:
	is_throwing = 1

just_released = 2
if mouse.events[bge.events.LEFTMOUSE] == UN_PRESSED and dict["spin_left"] == 2 and dict["spin_right"] == 2:
	just_released = 1
	
	# object
	new_projectile = bge.logic.getCurrentScene().addObject("thrown_spear", "spawn_spear", spear_lifetime)	
	# orientation
	orientation = 2
	
	if astronaut.worldOrientation[1][1] == -1:
		orientation = 1	
	if astronaut.worldOrientation[1][1] == 1:
		orientation = 0
	
	projectile_list = [new_projectile, orientation, 1]
	new_projectile.collisionCallbacks.append(lambda x: be_a_man(projectile_list, x))
	
	projectiles.append(projectile_list)

for projectile in projectiles:
	try:
		if projectile[1] == 0 and projectile[2] == 1:
			projectile[0].applyMovement([0, spear_speed, 0], False)
			projectile[0].applyRotation([-spear_gravity_spin, 0, 0], False)
		if projectile[1] == 1 and projectile[2] == 1:
			projectile[0].applyMovement([0, -spear_speed, 0], False)
			projectile[0].applyRotation([spear_gravity_spin, 0, 0], False)
			
	except:
		print("dogeye")
		projectiles.remove(projectile)
	
# |ANIMATION HANDLERS|

def loopAnim(name, start, finish, layer, priority, blendin):
	if name == dict["prev_loop"]:
		bones.playAction(name, start, finish, layer, priority)
	else:
		bones.playAction(name, start, finish, layer, priority, blendin)
	
	dict["prev_loop"] = name

# crouch
if dict["on_ground"] == 1 and is_crouched == 1:
	loopAnim("crouch", 0.0, 10.0, 1, 1, 5.0)

# idle
if dict["on_ground"] == 1 and dict["moving_left"] == 2 and dict["moving_right"] == 2 and is_crouched == 2:
	#bones.playAction("Idle", 0.0, 66.0, 1, 0, 10.0)
	loopAnim("Idle", 0.0, 66.0, 1, 1, 10.0)
	
# walk
if dict["on_ground"] == 1 and (dict["moving_left"] == 1 or dict["moving_right"] == 1):
	#bones.playAction("Walk Cycle", 0.4, 28.6, 1, 0, 5.0)
	loopAnim("Walk Cycle", 0.4, 28.6, 1, 1, 5.0)

# jump
if dict["on_ground"] == 2:
	#bones.playAction("jump", 0.0, 100.0, 1, 0, 5.0)
	loopAnim("jump", 0.0, 100.0, 1, 1, 5.0)
	
# throwing
if is_throwing == 1:
	loopAnim("arm back", 0.0, 10.0, 2, 1, 5.0)

if just_released == 1:
	loopAnim("arm release", 0.0, 15.0, 2, 1, 7.0)
	

	
# |CAMERA|

# Setting up camera tracking on the Y axis.
camera = objects["main_camera"]
bge.logic.getCurrentScene().active_camera = camera

# Tracking the astronaut.
tracking = astronaut

if astronaut.position.y - camera.position.y > camera_scene_width:
	camera.position.y = astronaut.position.y + camera_scene_width

if astronaut.position.y - camera.position.y < -camera_scene_width:
	camera.position.y = astronaut.position.y - camera_scene_width

if enable_constant_camera_tracking == 1:
	camera.position.y = astronaut.position.y
	
	
	
# |AI|

	
	
# clean up
dict["projectiles"] = projectiles
